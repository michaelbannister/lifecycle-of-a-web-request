Each of the subfolders here contains a Dockerfile and scripts to help demonstrate some of the issues I discuss in my presentation.

You will need Docker, bash and (ideally) openssl.

In each folder, first run `build.sh` and then `run.sh` – press Ctrl-C to stop & remove the running container.
