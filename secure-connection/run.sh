#!/usr/bin/env bash

docker run --detach --rm -p 443:443 --name secure-connection-demo secure-connection-demo:latest
sleep 1
docker cp secure-connection-demo:/certificates/root-ca.crt tmp/
echo "Root CA certificate copied to tmp/root-ca.crt"
openssl x509 -in tmp/root-ca.crt -subject -dates -noout || echo "Sorry, can't print certificate info"
cat <<EOF
  curl https://ok.127.0.0.1.xip.io
  curl --cacert tmp/root-ca.crt https://ok.127.0.0.1.xip.io
  curl --cacert tmp/root-ca.crt https://badname.127.0.0.1.xip.io
  curl --cacert tmp/root-ca.crt https://expired.127.0.0.1.xip.io
EOF
docker attach secure-connection-demo
