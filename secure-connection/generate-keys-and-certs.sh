#!/usr/bin/env sh

x509_datetime_now() {
  echo $(date -u '+%Y%m%d%M%S')Z
}
x509_datetime_now_plus_seconds() {
  echo $(( $(date -u '+%Y%m%d%M%S') + $1 ))Z
}

openssl genrsa -out root-ca.key 4096
openssl req -out root-ca.crt -x509 -new -nodes -key root-ca.key -sha256 -days 7 -subj "/C=GB/L=London/O=Demo Root CA/CN=Demo Company"


openssl genrsa -out ok.key 4096
openssl req -out ok.csr -new -key ok.key -subj "/C=GB/L=London/O=Forgetful Company/CN=ok.127.0.0.1.xip.io"
openssl x509 -out ok.crt -req -in ok.csr -CA root-ca.crt -CAkey root-ca.key -CAcreateserial -days 7

openssl genrsa -out expired.key 4096
openssl req -out expired.csr -new -key expired.key -subj "/C=GB/L=London/O=Forgetful Company/CN=expired.127.0.0.1.xip.io"
openssl x509 -out expired.crt -req -in expired.csr -CA root-ca.crt -CAkey root-ca.key -CAcreateserial -days -1

openssl genrsa -out badname.key 4096
openssl req -out badname.csr -new -key badname.key -subj "/C=GB/L=London/O=Forgetful Company/CN=differentname.127.0.0.1.xip.io"
openssl x509 -out badname.crt -req -in badname.csr -CA root-ca.crt -CAkey root-ca.key -CAcreateserial -days 7
