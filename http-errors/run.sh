#!/usr/bin/env bash

docker run --rm -it -p 8080:8080 -v $(pwd)/mappings:/app/mappings --name http-errors-demo http-errors-demo:latest
cat <<EOF
  curl localhost:8080/slow
  curl localhost:8080/empty
EOF
